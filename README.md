# docker-centos-apache-php71

Docker with CentOS 7, Apache 2, PHP 7.1

# Docker Installation
```bash
yum install docker
```
# Docker Configuration

autostart/first start:
```bash
systemctl enable docker

systemctl start docker
```
# Docker-Compose Installation

```bash
curl -L "https://github.com/docker/compose/releases/download/1.8.1/docker-compose-$(uname -s)-$(uname -m)" > /usr/bin/docker-compose
chmod +x /usr/bin/docker-compose
```
(check for latest Version, and paste !)

# Building Container

Run as ROOT !

```bash
docker-compose up -d --build
```
# Docker Bash

```bash
docker-compose exec apache /bin/bash
```

# Zend Skeleton

```bash
composer create-project -sdev zendframework/skeleton-application path/to/install
```

# GIT, SSH, DB installation

## SSH

A simple script will look for a ssh-key-pair in config/ssh/ if there is one, it will
inject them to the container. If not the script will generate new ones. The public-key
will be shown while the installation-process but you also find it int the container directory
/root/.ssh/.

## GIT

Uncomment the git-function in the Dockerfile, if you want to use it.
After that put your repo-url in (git/repository.sh).

## Database

Uncomment the database function if needed, put your database information and commands
in the config/db directory use the db-config.sh for config injection.